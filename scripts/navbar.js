(function() {

const navbar = document.getElementsByClassName("navbar")[0];
const menu_checkbox = document.getElementById("menu-checkbox");
const menu_button = document.getElementById("menu-button");
const menu = navbar.querySelector(".links");
const header = document.getElementById("header");

const nc = navbar.classList;
const ensure_sticky = () => nc.toggle("sticky", window.pageYOffset > 0);
const ensure_color = () => nc.toggle("color", window.pageYOffset >= 48);

window.addEventListener("scroll", ensure_sticky);
ensure_sticky();

if (header) {
	window.addEventListener("scroll", ensure_color);
	ensure_color();
}

document.addEventListener("click", (e) => {
	if (!menu_button.contains(e.target) &&
		!menu_checkbox.contains(e.target) &&
		!menu.contains(e.target))
	{
		menu_checkbox.checked = false;
	}
});

})()
