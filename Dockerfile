FROM dgvozdenovic/haunt:latest
MAINTAINER Dušan Gvozdenović

WORKDIR /tmp

RUN git clone -b v0.1.0 https://gitlab.com/dusan-gvozdenovic/guile-bibtex.git && \
    cd guile-bibtex && \
    ./bootstrap && \
    mkdir build && cd build && \
    ../configure --prefix=/usr && \
    make -j$(nproc) && \
    make install && \
    cd .. && \
    rm -rf guile-bibtex

WORKDIR /app

EXPOSE 8000
