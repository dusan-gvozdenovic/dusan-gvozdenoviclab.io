(use-modules (ice-9 match)
             (ice-9 regex)
             (srfi srfi-1)
             (srfi srfi-19)
             (srfi srfi-26)
             (haunt asset)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt html)
             (haunt utils)
             (haunt builder atom)
             (haunt builder assets)
             (haunt builder blog)
             (haunt reader skribe)
             (haunt reader)
             (haunt reader commonmark)
             (commonmark)
             (bibtex)
             (sxml match)
             (sxml transform)
             (syntax-highlight)
             (syntax-highlight scheme)
             (syntax-highlight xml)
             (syntax-highlight c))

(define %site-prefix (make-parameter ""))
(define (prefix-url url) (string-append (%site-prefix) url))

(define %author "Dušan Gvozdenović")
(define %email "dusan.gvozdenovic.99@gmail.com")
(define %theme-color "#223346")
(define %gitlab-url  "https://gitlab.com/dusan-gvozdenovic")
(define %feed-url    (prefix-url "/feed.xml"))
(define %email-url   (string-append "mailto:" %email))
(define %user-image  "me.jpg")

(define %navbar-links
  `(("Projects" ,%gitlab-url)
    ("Contact"  ,(prefix-url "contact.html"))))

(define (image-url image) (prefix-url (string-append "/images/" image)))

;; TODO
;; (define (thumb-url image) (prefix-url (string-append "/images/thumbs/" image)))
(define (thumb-url image) (image-url image))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (type "text/css")
            (href ,(prefix-url (string-append "/css/" name ".css"))))))

(define (javascript name)
  `(script (@ (src ,(prefix-url (string-append "/scripts/" name ".js"))))))

;; Joins non-empty strings into a single string delimited by `delimiter`.
(define* (join #:optional (delimiter "") . lst)
  (define (not-empty? str) (not (equal? str "")))
  (string-join (filter not-empty? lst) delimiter))

(define (class-join . lst) (apply join " " lst))

(define* (base-layout site body #:key title navbar-color?)
  `((doctype "html")
    (html (@ (lang "en"))
      (head
        (meta (@ (charset "utf-8")))
        ;; Chrome, Firefox OS and Opera
        (meta (@ (name "theme-color") (content ,%theme-color)))
        ;; Windows Phone
        (meta (@ (name "msapplication-navbutton-color") (content ,%theme-color)))
        ;; iOS Safari
        (meta (@ (name "apple-mobile-web-app-status-bar-style") (content ,%theme-color)))
        (meta (@ (name "viewport") (content "width=device-width, initial-scale=1, viewport-fit=cover")))
        (title ,(if title (string-append title " - " (site-title site)) (site-title site)))
        (link (@ (href "feed.xml")
                 (type "application/atom+xml")
                 (rel "alternate")
                 (title "Atom feed")))
        (link (@ (type "image/x-icon")
                 (rel "icon")
                 (href ,(image-url "favicon.ico"))))
        ;; CSS
        ,(stylesheet "fonts")
        ,(stylesheet "md-icons.min")
        ,(stylesheet "main")
        ,(stylesheet "main-dark")
        ,(stylesheet "main-print")
        ,(stylesheet "code")
        ,(stylesheet "code-dark")
        ,(stylesheet "code-print"))
      (body
        ,(navbar title navbar-color?)
        ,body
        ,footer
        ;; JavaScript
        ,(javascript "navbar")))))

(define (navbar-link title path active)
  `(a (@ (class ,(if active "active" "")) (href ,path)) ,title))

(define (navbar title color?)
  `(nav (@ (class ,(class-join "navbar" (if color? "color" ""))))
        (div (@ (class "container"))
            (a (@ (href "/") (class "title")) ,%author)
            (input (@ (id "menu-checkbox") (type "checkbox")))
            (label (@ (id "menu-button") (class "link mdi mdi-menu") (for "menu-checkbox")))
            (nav (@ (class "links"))
              ,(map (lambda (c) (navbar-link (car c) (cadr c) (equal? (car c) title)))
                %navbar-links)))))

(define footer
  `(footer (@ (id "footer"))
           (div (@ (class "container"))
                (span (@ (class "credit"))
                      "Built with "
                      (a (@ (href "https://dthompson.us/projects/haunt.html")) Haunt)
                      ", a static site generator written in "
                      (a (@ (href "https://www.gnu.org/software/guile/")) "Guile Scheme") ".")
                (div (@ (class "contact-buttons"))
                     (a (@ (class "mdi mdi-email") (href ,%email-url)))
                     (a (@ (class "mdi mdi-rss-box") (href ,%feed-url)))
                     (a (@ (class "mdi mdi-gitlab") (href ,%gitlab-url)))))))

(define (post-template post)
  (let* ((subtitle (post-ref post 'subtitle))
         (background-url (post-ref post 'background))
         (background-style-attr
           `(style
              ,(if background-url
                 (string-append "background-image: url('"
                                (image-url background-url) "');") ""))))
  `((header (@ (id "header") ,background-style-attr)
            (figure (@ (class "heading"))
                    (figcaption
                      (h1 ,(post-ref post 'title))
                      ,(if subtitle `(h2 ,subtitle) `())
                      (h3 ,(date->string* (post-date post)))
                      (h4 (@ (class "time-to-read mdi mdi-clock-outline"))
                          " " ,(minutes-to-read post) " minute read")
                      )))
    (main (@ (class "content container")) ,(post-sxml post)))))

(define first-paragraph
  (match-lambda
         ((_ *** ('p a ...)) `(p ,@a))
         (_ '())))

(define paragraphs
  (match-lambda
      (`(p . ,content) `(p ,@content))
      (((and (key *** `(p . ,content)) inner) . rest)
       (append (if (null? key)
		   (list `(p ,@content))
		   (paragraphs inner)) (paragraphs rest)))
      ((this . rest) (paragraphs rest))
      (any '())))

; BUG: For some reason incorrect
(define count-words
  (match-lambda
    (`((@ . ,ignore) . ,rest) (count-words rest))
    (((and (key *** `(,_ . ,content)) inner) . rest)
     (+ (count-words inner) (count-words rest)))
    ((this . rest) (+ (if (or (symbol? this) (string? this)) 1 0)
                          (count-words rest)))
    (any 0)))

(define (minutes-to-read post)
  (let ((mins (quotient (count-words (post-sxml post)) 220)))
    (if (> mins 0) mins 1)))

(define (post-preview post-uri post)
  (define post-thumbnail (post-ref post 'background))
  `(article (@ (class "post"))
        ,(if post-thumbnail
            `(div (@ (class "thumbnail"))
                  (img (@ (alt "article-description")
                          (class "img-responsive")
                          (src ,(thumb-url post-thumbnail))))) '())
        (div (@ (class "description"))
          (header (@ (class "header"))
              (h2 (@ (class "title"))(a (@ (href ,(post-uri post))) ,(post-ref post 'title)))
              (span (@ (class "date")) ,(date->string (post-date post) "~d ~B ~Y")))
          ,(first-paragraph (post-sxml post)))))

(define (collection-template site title posts prefix)
  (define (post-uri post)
    (string-append (or prefix "")
                   (site-post-slug site post) ".html"))
  `((header
      (@ (id "header") (class "home"))
      (figure (@ (class "heading"))
              (img (@ (id "user-image") (alt "user-image") (src ,(image-url %user-image))))
              (figcaption
                (h1 "Dušan's Website")
                (h3 "A place about tech & engineering"))))
    (main (@ (class "content container"))
          (section
            (h1 "Posts")
            ,@(map (lambda (post) (post-preview post-uri post)) posts)))))

(define* (ns-theme #:key (navbar-color? #f))
  (theme #:name "Novi Sad"
         #:layout
         (lambda (site title body)
           (base-layout site body
                        #:title title
                        #:navbar-color? navbar-color?))
         #:post-template post-template
         #:collection-template collection-template))

(define (static-page navbar-color? title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout
                  (ns-theme #:navbar-color? navbar-color?)
                  site title body)
               sxml->html)))

(define contact-page
  (static-page #t
    "Contact"
    "contact.html"
    `((div (@ (class "page-top-margin")))
      (main (@ (class "container content empty contact"))
            (div
              (h1 (@ (class "title")) "Contact")
              (div (@ (class "info"))
                   (span (@ (class "mdi mdi-email")))
                   (strong "E-Mail:")
                   (a (@ (href ,%email-url)) ,%email))
              (div (@ (class "info"))
                   (span (@ (class "mdi mdi-phone")))
                   (strong "Wire:")
                   (span "@Dgvozdenovic"))
              (div (@ (class "info"))
                   (span (@ (class "mdi mdi-telegram")))
                   (strong "Telegram:")
                   (span "@Dgvozdenovic")))
            (span (@ (class "icon mdi mdi-at")))))))

(define 404-page
  (static-page #t
    "404 - Not Found"
    "404.html"
    `((div (@ (class "page-top-margin")))
      (main (@ (class "container content empty"))
            (div
              (h1 (@ (class "title")) "404")
              (div (@ (class "info")) "The page you requested could not be found.")
              (a (@ (class "info") (href "/")) "Go Home >>>"))
            (span (@ (class "icon mdi mdi-cloud-alert")))))))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                      ('scheme lex-scheme)
                      ('xml    lex-xml)
                      ('c      lex-c)
                      (_ #f))))
    (if lexer
      (highlights->sxml (highlight lexer source))
      source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
        (let ((lang (string->symbol
                    (string-drop class (string-length "language-")))))
        `(code (@ ,@attrs) ,(maybe-highlight-code lang source))))
    (,other other)))

;; TODO: Simplify this
(define (post-process-image . tree)
  (sxml-match tree
    ((img (@ (src ,src) (alt ,alt) . ,attrs))
      (if (string-suffix? ".svg" src)
        `(object (@ (type "image/svg+xml")
                    (data ,src)
                    (class "img-responsive")) (img (@ (src ,src) (alt ,alt))))
        `(img (@ (src ,src) (alt ,alt) (class "img-responsive")) ,attrs)))
    (,other other)))

(define (post-process-anchor . tree)
  (define (ref? content)
    (let* ((text (fold-right string-append "" content))
           (fst  (string-ref text 0))
           (lst  (string-ref text (- (string-length text) 1))))
           (and (eqv? fst #\[) (eqv? lst #\]))))
  (sxml-match tree
    ((a (@ . ,attrs) . ,content) (guard (ref? content))
     `(sup (a (@ ,@attrs) ,content)))
    (,other other)))

(define %commonmark-rules
  `((code . ,highlight-code)
    (img . ,post-process-image)
    (a . ,post-process-anchor)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define (post-process-bibtex sxml)
  (let ((database 'not-implemented-yet!))
    sxml
))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
    (lambda (file)
      (call-with-input-file file
        (lambda (port)
          (values (read-metadata-headers port)
                  ((compose post-process-commonmark post-process-bibtex)
                    (commonmark->sxml port))))))))

(site #:title %author
      #:domain "gvozdenovic.io"
      #:default-metadata
      `((author . ,%author)
        (email  . ,%email))
      #:build-directory "public"
      #:readers (list (make-skribe-reader
                        #:modules '((haunt skribe utils)))
                      html-reader
                      commonmark-reader*)
      #:builders (list (blog #:theme (ns-theme))
                       contact-page
                       404-page
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (static-directory "fonts")
                       (static-directory "images")
                       (static-directory "css")
                       (static-directory "scripts")))
