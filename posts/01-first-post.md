title: First Post
date:  2020-04-13 11:00
---

# Hi.
I've been trying to start a personal website for quite some time now.
The main reason for that is to write about cool but niché things people rarely
talk about.
My writing skills could use some improvement as well.
Finally, I think I've been lacking an appropriate platform to showcase my
projects. I mean, GitLab is awesome *as-it-is* but I feel that people
might discover them more easily here.

# How?
Recently, I've stumbled upon [Haunt](https://dthompson.us/projects/haunt.html)
which just might be the best static site generator ever!
Being written in Guile, it uses a simple, elegant syntax yet it's very robust.
Some things it allows you to do are:

- Pass around markup as S-expressions
- Pattern match against elements and modify them
- Intercept and tweak almost anything in its pipeline!

A couple of websites that (unsuprisingly) use it are [Guix](https://guix.gnu.org/)
and [Guile's official website](https://www.gnu.org/software/guile/).

## I wanna try it!
In case Haunt is not in your distro's repos you can either build it from source
as the official guide suggests or use the [Docker image](https://hub.docker.com/r/dgvozdenovic/haunt)
I've created.